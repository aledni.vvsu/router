<?php

class Route
{
    public string $filepath; // Путь до файла, соответствующего маршруту
    public array $params = []; // Динамческие части маршрута
}
